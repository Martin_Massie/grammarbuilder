package com.DIYLG;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import com.DIYLG.table.*;
import com.DIYLG.token.Token;

public abstract class Transducer {

	protected Token EOF, token;
	
	protected Integer tableNumber = 1;
	
	protected Boolean keep, node, read, stack;
	
	protected String keptCharacters = "";
	
	protected HashMap<Integer, AbstractTable> tables;
	
	public Transducer(){
		tables = new HashMap<>();
	}
	
	protected AbstractTable addTable(AbstractTable table){
		tables.put(table.getId(), table);
		return table;
	}
	
	protected AbstractTable removeTable(AbstractTable table){
		tables.remove(table.getId());
		return table;
	}
	protected AbstractTable removeTable(Integer tableId){
		AbstractTable table = tables.get(tableId);
		tables.remove(tableId);
		return table;
	}
	
	protected AbstractTable getTable(Integer tableId){
		return tables.get(tableId);
	}
	
	protected boolean populateTables(Stream<String> stream){
		EOF = buildEOF();
		stream.forEach(line -> {
			line = line.trim();
			if(line != null){
				switch(line.substring(0, 1)){
				case "(":
					line = line.substring(1);
					AbstractTable table = buildTable(line);
					if(table != null){
						addTable(table);
					}
					break;
				case "^":
					EOF = buildEOF();
				}
			}
		});
		
		return true;
	}
	
	private AbstractTable buildTable(String line){
		List<String> elements = Arrays.asList(line.split(" "));
		AbstractTable theTable = null;
		switch(elements.get(0)){
		case "ScannerReadaheadTable":
			theTable = new ScannerReadaheadTable(line, this);
			break;
		case "SemanticTable":
			theTable = new SemanticTable(line, this);
			break;
		case "ReadaheadTable":
			theTable = new ReadaheadTable(line, this);
			break;
//		case "ReadbackTable":
//			theTable = new ReadbackTable(line, this);
//			break;
//		case "ShiftBackTable":
//			theTable = new ShiftbackTable(line, this);
//			break;
//		case "ReduceTable":
//			theTable = new ReduceTable(line, this);
//			break;
//		case "AcceptTable":
//			theTable = new AcceptTable(line, this);
//			break;
		}
		
		return theTable;
	}
	
	protected abstract Token buildEOF();
	public abstract void keep(Object aChar);
	protected abstract Integer runTables(Integer tableNumber);
	
	public Token buildToken(String symbol, String id){
		return new Token(id, symbol);
	}

	public abstract Token peek();
	
	public abstract boolean next();

	public abstract void semanticAction(String action, String parameter);

	public Token getEOF() {
		return EOF;
	}

	public void setEOF(Token eOF) {
		EOF = eOF;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}
}
