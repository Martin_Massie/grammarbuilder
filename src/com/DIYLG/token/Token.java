package com.DIYLG.token;

public class Token {

	protected String label;
	protected String symbol;
	
	public Token(String aLabel, String aSymbol){
		label = aLabel;
		symbol = aSymbol;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	public String toString(){
		return "Label: " + label + " Symbol: " + symbol;
	}
	
	public boolean equals(Object ob){
		Token other = (Token) ob;
		return other.label.equals(this.label) && other.symbol.equals(this.symbol);
	}
}
