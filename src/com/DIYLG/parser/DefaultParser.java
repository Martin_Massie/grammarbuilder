package com.DIYLG.parser;

import com.DIYLG.table.AbstractTable;
import com.DIYLG.table.SemanticTable;
import com.DIYLG.token.Token;

public class DefaultParser extends AbstractParser{

	public DefaultParser(){
		super();
	}

	@Override
	public void keep(Object aChar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void semanticAction(String action, String parameter) {
		System.out.println("SemanticAction: " + action);
	}

	@Override
	protected Integer runTables(Integer number) {
		AbstractTable table = tables.get(number);
		if(table == null)return -2;
		number = table.runTable();
		switch(runTables(number)){
			case -1:
				return -1;//syntax
			case -2:
				return -2;//eof
			default:
				return number;
		}
	}

	@Override
	public Token peek() {
		return scanner.peek();
	}

	@Override
	public boolean next() {
		if(token != scanner.getEOF()){
			scanner.next();
			System.out.println(scanner.peek());
			return true;
		}
		return false;
	}

	@Override
	public void accept() {
		accepted = true;
		
	}
}
