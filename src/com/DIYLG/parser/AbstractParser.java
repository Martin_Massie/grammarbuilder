package com.DIYLG.parser;

import java.util.Stack;

import javax.swing.tree.DefaultMutableTreeNode;

import com.DIYLG.Transducer;
import com.DIYLG.scanner.AbstractScanner;
import com.DIYLG.token.Token;

public abstract class AbstractParser extends Transducer {

	protected AbstractScanner scanner;
	protected Boolean accepted;
	
	protected Stack<Integer> tableStack;
	protected Stack<Token> tokenStack;
	protected DefaultMutableTreeNode treeRoot;
	protected Stack<DefaultMutableTreeNode> treeStack;
	private int right, left;

	public AbstractParser(){
		super();
		accepted = false;
		treeRoot = getNewNode(null);
		
		tableStack = new Stack<>();
		tokenStack = new Stack<>();
		treeStack = new Stack<>();
		
		right = 0;
		left = 1;
	}
	
	public DefaultMutableTreeNode getNewNode(Object node) {
		return new DefaultMutableTreeNode(node);
	}
	
	protected void setNewRoot(DefaultMutableTreeNode... newChild) {
		DefaultMutableTreeNode root = getNewNode(null);
		for(DefaultMutableTreeNode child : newChild){
			root.add(child);
		}
		treeRoot = root;
	} 

	protected Token buildEOF(){
		return new Token("EndOfFile", "EndOfFile");
	}
	
	public AbstractScanner getScanner() {
		return scanner;
	}

	public void setScanner(AbstractScanner scanner) {
		this.scanner = scanner;
	}

	public void accept(){
		accepted = true;
	}
	
	public void pushToken(Token token){
		tokenStack.push(token);
		right +=1;
		left = right + 1;
	}
	
	public void pushTable(Integer number){
		tableStack.push(number);
	}
	
	public void pushTree(DefaultMutableTreeNode tree){
		treeStack.push(tree);
	}
	
	public DefaultMutableTreeNode parse(){
		while(!accepted){
			tableNumber = 1;
			next();
			runTables(tableNumber);
		accept();
		}
		return treeRoot;
	}
}
