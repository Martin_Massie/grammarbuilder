package com.DIYLG.tree.traverser;

import com.DIYLG.tree.Node;
import com.DIYLG.tree.Tree;

public class SimpleTraverser extends AbstractTraverser {
	
	public SimpleTraverser(Tree theTree){
		tree = theTree;
//		stack = new Stack();
//		stack.push(tree.getRoot());
		next = tree.getRoot();
		previous = null;
	}
	
	@Override
	public boolean hasNext(){
		
		return false;
	}
	
	@Override
	public Node getNext(){
		
		return next;
	}

	@Override
	public Node getPrevious() {
		// TODO Auto-generated method stub
		return null;
	}
}
