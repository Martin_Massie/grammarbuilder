package com.DIYLG.tree.traverser;

import com.DIYLG.tree.Node;
import com.DIYLG.tree.Tree;

public abstract class AbstractTraverser {

	@SuppressWarnings("rawtypes")
	protected Tree tree;
	
	@SuppressWarnings("rawtypes")
	protected Node next, previous;

	public Tree getTree() {
		return tree;
	}

	public void setTree(Tree tree) {
		this.tree = tree;
	}

	public abstract Node getNext();

	public abstract Node getPrevious();
	
	public abstract boolean hasNext();
}
