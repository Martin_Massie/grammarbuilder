package com.DIYLG.tree;

import com.DIYLG.tree.traverser.SimpleTraverser;

public class Tree<T> {
	
	private Node<T> root;
	
	private SimpleTraverser traverser;
	
	public Tree(Node<T> theRoot){
		root = theRoot;
	}
	
	public Node<T> add(Node<T> aNode){
		
		return aNode;
	}
	
	public Node<T> getRoot(){
		return root;
	}
	
	public void setRoot(Node<T> aRoot){
		root = aRoot;
	}
}
