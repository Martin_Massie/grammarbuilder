package com.DIYLG.tree;

import java.util.ArrayList;

public class Node<T> {

	private ArrayList<Node<T>> children;
	private T data;
	private Node<T> parent;
	
	public Node(){
		children = new ArrayList<>();
	}
	
	public Node(Node<T> aParent){
		parent = aParent;
	}
	
	public Node(T someData){
		data = someData;
	}
	
	public Node(Node<T> aParent, T someData){
		parent = aParent;
		data = someData;
	}
	
	public Node(Node<T> aParent, T someData, ArrayList<Node<T>> theChildren){
		parent = aParent;
		data = someData;
		children = new ArrayList<Node<T>>(theChildren);
	}

	public ArrayList<Node<T>> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Node<T>> children) {
		this.children = children;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Node<T> getParent() {
		return parent;
	}

	public void setParent(Node<T> parent) {
		this.parent = parent;
	}
	
	public Node<T> addChild(Node<T> aChild){
		children.add(aChild);
		return aChild;
	}
	
	public Node<T> removeChild(Node<T> aChild){
		if(children.contains(aChild)){
			children.remove(aChild);
			return aChild;
		}
		return null;
	}
}
