package com.DIYLG;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFileChooser;

import com.DIYLG.parser.DefaultParser;
import com.DIYLG.scanner.AbstractScanner;
import com.DIYLG.scanner.DefaultScanner;

public class GrammarBuilder {
	
	private Grammar grammar;
	
	private DefaultParser parser;
	
	private DefaultScanner scanner;
	
	public GrammarBuilder(){
		grammar = new Grammar();
		parser = new DefaultParser();
		grammar.setParser(parser);
		scanner = new DefaultScanner();
		parser.setScanner(scanner);
		try {
			scanner.populateTables(Files.lines(Paths.get("resources/files/scannerTables.st")));
		} catch (IOException e) {
			System.out.println("Scanner Tables Population Error");
		}
		try {
			parser.populateTables(Files.lines(Paths.get("resources/files/parserTables.st")));
		} catch (IOException e) {
			System.out.println("Parser Tables Population Error");
		}
		System.out.println(parser.tables);
	}
	
	public Grammar getGrammar() {
		return grammar;
	}

	public void setGrammar(Grammar grammar) {
		this.grammar = grammar;
	}

	public DefaultParser getParser() {
		return parser;
	}

	public void setParser(DefaultParser parser) {
		this.parser = parser;
	}

	public DefaultScanner getScanner() {
		return scanner;
	}

	public void setScanner(DefaultScanner scanner) {
		this.scanner = scanner;
	}

	public static void main(String[] args){
		GrammarBuilder gb = new GrammarBuilder();
		AbstractScanner scanner = gb.getScanner();
		JFileChooser fc = new JFileChooser();
		fc.showOpenDialog(null);
		try {
			scanner.scan(new String(Files.readAllBytes(fc.getSelectedFile().toPath())));
			gb.getParser().setScanner(scanner);
			gb.getParser().parse();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
