package com.DIYLG.table;

import java.util.HashMap;

import com.DIYLG.Transducer;
import com.DIYLG.parser.AbstractParser;
import com.DIYLG.token.Token;

public class ReadaheadTable extends AbstractTable{

	private AbstractParser transducer;
	
	public ReadaheadTable(String line, Transducer transducer) {
		this.transducer = (AbstractParser) transducer;
		transitions = new HashMap<>();
		String[] tableInfo = line.substring(0, line.indexOf("(")).split(" ");
		id = new Integer(tableInfo[1]);
		line = line.substring(line.indexOf("(") + 1);
		String working = "";
		while(line.contains(") (")){
			working = line.substring(0, line.indexOf(") ("));
			parseTransition(working);
			line = line.substring(line.indexOf(") (") + 3);
		}
		working = line.substring(0, line.indexOf("))"));
		parseTransition(working);
		System.out.println(id + transitions.toString());
	}

	private void parseTransition(String working) {
		String[] elements;
		Transition trans;
		elements = working.split(" ");
		trans = new Transition(elements[1], Integer.valueOf(elements[2]));
		addTransition(elements[0], trans);
	}

	@Override
	public Integer runTable() {
		Token aToken = transducer.peek();
		System.out.println(id);
		Transition trans = transitions.get(aToken.getSymbol());
		String actions = trans.getActions();
		if(actions.contains("S")){
			transducer.pushTable(trans.getSuccessor());
			if(actions.contains("N")){
				transducer.pushTree(transducer.getNewNode(aToken));
			}else{
				transducer.pushTree(transducer.getNewNode(null));
			}
		}
		if(actions.contains("R")){
			if(transducer.next()){
				return trans.getSuccessor();
			}else{
				return -2;
			}
		}
		if(actions.contains("L")){
			return trans.getSuccessor();
		}
		return -1;
	}

}
