package com.DIYLG.table;

import java.util.HashMap;
import java.util.stream.IntStream;

import com.DIYLG.Transducer;
import com.DIYLG.scanner.AbstractScanner;

public class ScannerReadaheadTable extends AbstractTable{
	
	public AbstractScanner transducer;
	
	public ScannerReadaheadTable(String line, Transducer transducer){
		this.transducer = (AbstractScanner) transducer;
		transitions = new HashMap<>();
		String[] tableInfo = line.substring(0, line.indexOf("(")).split(" ");
		id = new Integer(tableInfo[1]);
		line = line.substring(line.indexOf("(") + 1);
		String working = "";
		while(line.contains(") (")){
			working = line.substring(0, line.indexOf(") ("));
			parseTransition(working);
			line = line.substring(line.indexOf(") (") + 3);
		}
		working = line.substring(0, line.indexOf("))"));
		parseTransition(working);
	}
	
	private void parseTransition(String working) {
		String[] elements;
		Transition trans;
		switch(working.substring(0, 1)){
		case "(":
			elements = working.split("\\)");
			String[] transElements = elements[1].split("' ");
			trans = new Transition(transElements[0], Integer.valueOf(transElements[1]));
			String[] states = elements[0].substring(1).split(" to: ");
			Integer start, end;
			if(states[0].contains("$")){
				start = (int)states[0].charAt(1);
				end = (int)states[1].charAt(1);
			}else{
				start = Integer.valueOf(states[0]);
				end = Integer.valueOf(states[1]);
			}
			for(int i = start; i <= end; i ++){
				addTransition(i, trans);
			}
			break;
		case "$":
			elements = working.split(" ");
			trans = new Transition(elements[1], Integer.valueOf(elements[2]));
			addTransition((int)elements[0].charAt(1), trans);
			break;
		case "'":
			elements = working.split(" ");
			trans = new Transition(elements[1], Integer.valueOf(elements[2]));
			IntStream chars = elements[0].substring(1, elements[0].length() - 1).chars();
			chars.forEach( aChar -> {
				addTransition(aChar, trans);
			});
			break;
		default://starts with number
			elements = working.split(" ");
			trans = new Transition(elements[1], Integer.valueOf(elements[2]));
			addTransition(Integer.valueOf(elements[0]), trans);
			break;
		}
	}
	

	@Override
	public Integer runTable() {
		int aChar = transducer.peekChar();
		Transition trans = transitions.get(aChar);
		String actions = trans.getActions();
		if(actions.contains("K")){
			transducer.keep((char)aChar);
		}
		if(actions.contains("R")){
			if(transducer.nextChar()){
				return trans.getSuccessor();
			}else{
				return -2;
			}
		}
		if(actions.contains("L")){
			return trans.getSuccessor();
		}
		return -1;
	}
}
