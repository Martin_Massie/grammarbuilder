package com.DIYLG.table;

import com.DIYLG.Transducer;

public class SemanticTable extends AbstractTable{

	private String action, symbol, next;
	
	public SemanticTable(String line, Transducer transducer) {
		this.transducer = transducer;
		String[] elements = line.substring(0, line.length() - 1).split(" ");
		id = Integer.valueOf(elements[1]);
		action = elements[2];
		symbol = elements[3];
		next = elements[4];
	}

	@Override
	public Integer runTable() {
		transducer.semanticAction(action, symbol);
		return Integer.valueOf(next);
	}

}
