package com.DIYLG.table;

import java.util.HashMap;

import com.DIYLG.Transducer;

public abstract class AbstractTable {

	protected Integer id;
	
	protected HashMap<Object, Transition> transitions;
	
	protected Transducer transducer;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	protected void addTransition(Object symbol, Transition transition){
		transitions.put(symbol, transition);
	}
	
	public abstract Integer runTable();
}
