package com.DIYLG.table;

import com.DIYLG.Transducer;
import com.DIYLG.parser.AbstractParser;

public class AcceptTable extends AbstractTable {

	private AbstractParser transducer;
	
	public AcceptTable(String line, Transducer transducer) {
		this.transducer = (AbstractParser) transducer;
		String[] elements = line.substring(0, line.length() - 1).split(" ");
		id = Integer.valueOf(elements[1]);
	}

	@Override
	public Integer runTable() {
		transducer.accept();
		return -1;
	}

}
