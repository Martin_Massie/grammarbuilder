package com.DIYLG.table;

public class Transition {

	private String actions;
	private Integer successor;
	
	public Transition(String actions, Integer nextState) {
		this.actions = actions;
		successor = nextState;
	}
	public String getActions() {
		return actions;
	}
	public void setActions(String action) {
		this.actions = action;
	}
	public Integer getSuccessor() {
		return successor;
	}
	public void setSuccessor(Integer successor) {
		this.successor = successor;
	}
	
	public String toString(){
		return "Actions: " + actions + " Successor: " + successor;
	}
}
