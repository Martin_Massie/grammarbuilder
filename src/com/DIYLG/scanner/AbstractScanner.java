package com.DIYLG.scanner;

import com.DIYLG.Transducer;
import com.DIYLG.token.Token;

public abstract class AbstractScanner extends Transducer {

	protected String keptCharacters = "";
	protected Integer left;
	protected Integer right;
	
	protected Token buildEOF(){
		
		return new Token("256","EndOfFile");
	}

	public abstract void scan(String string);

	public abstract boolean nextChar();

	public abstract int peekChar();
}
