package com.DIYLG.scanner;

import com.DIYLG.table.AbstractTable;
import com.DIYLG.table.SemanticTable;
import com.DIYLG.token.Token;

public class DefaultScanner extends AbstractScanner {
	
	private Screener screener;
	
	private char[] input;
	
	private Integer pointer;
	
	public DefaultScanner(){
		screener = new Screener();
		keptCharacters = "";
		pointer = 0;
	}

	@Override
	public void keep(Object aChar) {
		keptCharacters += (char) aChar;
	}

	@Override
	protected Integer runTables(Integer number) {
		AbstractTable table = tables.get(number);
		if(table == null)return -2;
		number = table.runTable();
		if(table.getClass() != SemanticTable.class){
			switch(runTables(number)){
				case -1:
					return -1;
				case -2:
					token = EOF;
					return -1;
				default:
					return number;
			}
		}else{
			tableNumber = number;
			return -1;
		}
	}
	
	public void semanticAction(String action, String parameter) {
		switch(action){
		case "buildToken:":
			token = screener.screen(buildToken(parameter, keptCharacters));
			keptCharacters = "";
		}
	}

	@Override
	public Token peek() {
		return token;
	}

	@Override
	public boolean next() {
		runTables(tableNumber);
		return true;
	}
	
	public boolean nextChar(){
		pointer += 1;
		return (pointer < input.length);
	}
	
	public int peekChar(){
		return input[pointer];
	}
	
	public void scan(String input){
		this.input = input.toCharArray();
	}
}
