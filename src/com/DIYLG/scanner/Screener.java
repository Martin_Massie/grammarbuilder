package com.DIYLG.scanner;

import java.util.ArrayList;
import java.util.Arrays;

import com.DIYLG.token.Token;

public class Screener {
	
	private final ArrayList<String> keyWords = new ArrayList<>(Arrays.asList(
		"read",
		"look",
		"keep",
		"noKeep",
		"stack",
		"noStack",
		"node",
		"noNode"));

	public Token screen(Token token){
		if(keyWords.contains(token.getLabel())){
			return new Token(token.getLabel(), token.getLabel());
		}
		return token;
	}
}
