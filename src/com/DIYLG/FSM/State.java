package com.DIYLG.FSM;

import java.util.HashMap;

import com.DIYLG.table.Transition;

public class State {
	
	private Boolean isStart = false;
	private boolean isEnd = false;

	private HashMap<String, Transition> transitions;
	
	public State(){
		transitions = new HashMap<>();
	}
	public State(Boolean isStart, Boolean isEnd){
		transitions = new HashMap<>();
		this.isStart = isStart;
		this.isEnd = isEnd;
	}
	
//	public void addTransition(String label, State nextState){
//		transitions.put(label, new Transition(label, nextState));
//	}
	
	public Transition removeTransition(Transition transition){
		return transitions.remove(transition.getActions());
	}
	
	public Transition removeTransition(String label){
		return transitions.remove(label);
	}
	
//	public State triger(String label){
//		return transitions.get(label).getSuccessor();
//	}
	
	public Boolean isStart(){
		return isStart;
	}
	public void setStart(Boolean isStart) {
		this.isStart = isStart;
	}
	public boolean isEnd() {
		return isEnd;
	}
	public void setEnd(boolean isEnd) {
		this.isEnd = isEnd;
	}
}
